# Contributing to Wave Optics

Thanks for your interest in _Wave Optics_. Our goal is to bring fast, open-source computational 
electromagnetics software to all research and educational communities.

This guide details how to use issues and pull requests to improve GitLab recipes.

Please stick as close as possible to the guidelines. That way we ensure quality guides
and easy to merge requests.

## Branch Organisation
The main development has only feature branches and a master branch. In addition, there are the
following branches

### Trunk branch
Origin/master is the main branch where the source code of HEAD always reflects a production-ready 
state.

### Feature branch
The feature branch workflow assumes a central repository, and master represents the official 
project history. Instead of committing directly on their local master branch, developers create 
a new branch every time they start work on a new feature. Feature branches should have 
descriptive names, like animated-menu-items or issue-#1061. The idea is to give a clear, 
highly-focused purpose to each branch. Git makes no technical distinction between the master 
branch and feature branches, so developers can edit, stage, and commit changes to a feature 
branch.
 
### Environment branch
Once the features are implemented and considered fairly stable, they get merged into the staging 
branch and then automatically deployed to the Staging environment. This is when quality assurance
kicks in: testers go to staging servers and verify that the code works as intended.

It is very handy to have a separate branch called staging to represent your staging environment. 
It will allow developers to deploy multiple branches to the same server simultaneously, simply by 
merging everything that needs to be deployed to the staging branch. It will also help testers 
understand what exactly is on staging servers at the moment, just by looking inside the staging 
branch.

### Production branch
Once the feature is implemented and tested, it can be deployed to production. If the feature was 
implemented in a separate branch, it should be merged into master branch. The 
branches should be deleted after they are merged to avoid confusion between team members.

The next step is to make a diff between the production and development branches to take a quick
look at the code that will be deployed to production. This gives you one last chance to spot 
something that’s not ready or not intended for production. Stuff like debugger breakpoints,
verbose logging or incomplete features.

Once the diff review is finished, you can merge the development branch into production and then
initialize a deployment of the production branch to your Production environment by hand. Specify 
a meaningful message for your deployment so that your team knows exactly what you deployed.


### Release branch
This branch only contains only the minor versions. The stable branch uses master as a starting 
point and is created as late as possible. By branching as late as possible you minimize the time 
you have to apply bug fixes to multiple branches. 
After a release branch is announced, only serious bug fixes are included in the release branch. 
If possible these bug fixes are first merged into master and then cherry-picked into the release 
branch. This way you can't forget to cherry-pick them into master and encounter the same bug on 
subsequent releases. 

## Semantic Versioning
*Wave Optics* follows semantic versioning. We release patch versions for bugfixes, minor versions
for new features, and major versions for any breaking changes. When we make breaking changes, we 
also introduce deprecation warnings in a minor version so that our users learn about the upcoming 
changes and migrate their code in advance.

We tag every pull request with a label marking whether the change should go in the next patch, 
minor, or a major version. We release new patch versions every few weeks, minor versions every 
few months, and major versions one or two times a year.

Every significant change is documented in the changelog file.

## Bugs
### Reporting bugs
This section guides you through submitting a bug report for *Wave Optics*. Following these 
guidelines helps maintainers and the community understand your report, reproduce the behavior and
find related reports.

Before creating bug reports, please check this list as you might find out that you don't need to 
create one. When you are creating a bug report, please include as many details as possible. Fill 
out the required template, the information it asks for helps us resolve issues faster.

## contributions
*Wave Optics* welcomes contributions from everyone.

Contributions to _Wave Optics_ should be made in the form of Gitlab merge requests. Each merge request will
be reviewed by a core contributor (someone with permission to land patches) and either landed in the
main tree or given feedback for changes that would be required.

### Proposing a change

### Your first pull request
- Branch from the master branch and, if needed, rebase to the current master
  branch before submitting your pull request. If it doesn't merge cleanly with
  master you may be asked to rebase your changes.

- Commits should be as small as possible, while ensuring that each commit is
  correct independently (i.e., each commit should compile and pass tests). 

- Don't put submodule updates in your pull request unless they are to landed
  commits.

- If your patch is not getting reviewed or you need a specific person to review
  it, you can @-reply a reviewer asking for a review in the pull request or a
  comment.

- Add tests relevant to the fixed bug or new feature.  

### Sending a pull request

### Benchmark Your Code

For changes that might have an impact on performance, please benchmark your code and measure the 
impact. Please share the benchmark script you used as well as the results. You should consider 
including this information in your commit message, which allows future contributors to easily 
verify your findings and determine if they are still rel

It is very easy to make an optimization that improves performance for a specific scenario you 
care about but regresses on other common cases. Therefore, you should test your change against a 
list of representative scenarios. Ideally, they should be based on real-world scenarios extracted 
from production applications.

### Style guide
To be developed ...

## Code of conduct

As contributors and maintainers of this project, we pledge to respect all
people who contribute through reporting issues, posting feature requests,
updating documentation, submitting pull requests or patches, and other
activities.

We are committed to making participation in this project a harassment-free
experience for everyone, regardless of level of experience, gender, gender
identity and expression, sexual orientation, disability, personal appearance,
body size, race, ethnicity, age, or religion.

Examples of unacceptable behavior by participants include the use of sexual
language or imagery, derogatory comments or personal attacks, trolling, public
or private harassment, insults, or other unprofessional conduct.

Project maintainers have the right and responsibility to remove, edit, or
reject comments, commits, code, wiki edits, issues, and other contributions
that are not aligned to this Code of Conduct. Project maintainers who do not
follow the Code of Conduct may be removed from the project team.

This code of conduct applies both within project spaces and in public spaces
when an individual is representing the project or its community.

Instances of abusive, harassing, or otherwise unacceptable behavior can be
reported by email shashank@bandari.in 

This Code of Conduct is adapted from the Contributor Covenant, version 1.1.0,
available at http://contributor-covenant.org/version/1/1/0/.

## Feedback
You're encouraged to help improve the quality of this guide.

Please contribute if you see any typos or factual errors. To get started, you can read our 
documentation contributions section.

You may also find incomplete content or stuff that is not up to date. Please do add any missing 
documentation for master. Make sure to check guides first to verify if the issues are 
already fixed or not on the master branch.

If for whatever reason you spot something to fix but cannot patch it yourself, please open an 
issue.

And last but not least, any kind of discussion regarding Ruby on Rails documentation is very 
welcome on the link:https://groups.google.com/forum/#!forum/wave-optics[wave-optics] mailing list.

