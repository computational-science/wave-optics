#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <assert.h>


#include <util/huffman.h>

struct HuffmanTest
{
  int *matrix;


};


static void huffman_get_symbol_count_test(void **state)
{
  unsigned int nmat;
  struct HuffmanTest * huffman = *state;

  nmat = huffman_get_symbol_count(huffman->matrix, 9);
  assert_int_equal(4, nmat);
}

static void huffman_get_frequency_test(void **state)
{
  unsigned int nmat;
  struct HuffmanTest * huffman = *state;
  float *frequencies;

  nmat = huffman_get_symbol_count(huffman->matrix, 9);
  frequencies = huffman_get_frequencies(huffman->matrix, 9, nmat);
  assert((*(frequencies+0) > 0.333) && (*(frequencies + 0) < 0.334));
  assert((*(frequencies+1) > 0.222) && (*(frequencies + 1) < 0.223));
  assert((*(frequencies+2) > 0.111) && (*(frequencies + 2) < 0.112));
  assert((*(frequencies+3) > 0.333) && (*(frequencies + 3) < 0.334));

  free(frequencies);
}

static void huffman_get_tree_test(void **state)
{
  unsigned int nmat;
  struct HuffmanTest * huffman = *state;
  float *frequencies;
  struct huffman_node_t * node;
  char val[8];

  nmat = huffman_get_symbol_count(huffman->matrix, 9);
  frequencies = huffman_get_frequencies(huffman->matrix, 9, nmat);
  node = get_huffman_tree(frequencies, nmat);
  sscanf(val, "%f", node->frequency);
  assert_string_equal(val, "1.0000");
  assert(node->frequency > 0.9999);
  free(frequencies);
}


static int
setUp(void ** state)
{
  struct HuffmanTest *huffman = calloc(9, sizeof(struct HuffmanTest));
  huffman->matrix = (int *)malloc(sizeof(int)*9);
  *state = huffman;

  *(huffman->matrix + 0) = 1;
  *(huffman->matrix + 1) = 0;
  *(huffman->matrix + 2) = 3;
  *(huffman->matrix + 3) = 3;
  *(huffman->matrix + 4) = 0;
  *(huffman->matrix + 5) = 3;
  *(huffman->matrix + 6) = 2;
  *(huffman->matrix + 7) = 0;
  *(huffman->matrix + 8) = 1;
  /* Frequencies are as follows:
   * 0 : 3 -> 33.3#
   * 1 : 2 -> 22.2%
   * 2 : 1 -> 11.1%
   * 3 : 3 -> 33.3%
   * */
  return 0;
}



static int 
tearDown(void ** state)
{
  struct HuffmanTest * huffman = *state;
  free(huffman);
  return 0;
}


int main(int argc, char *argv[])
{
  const struct CMUnitTest tests[] = {
    cmocka_unit_test(huffman_get_symbol_count_test),
    cmocka_unit_test(huffman_get_frequency_test),
    cmocka_unit_test(huffman_get_tree_test),
  };

  return cmocka_run_group_tests(tests, setUp, tearDown);
}
