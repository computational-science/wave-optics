#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <assert.h>


#include <util/list.h>



struct DataNode
{
  int val;
};

struct ListState
{
  List *list;
};

static void 
list_append_test(void **state)
{

}

static void 
list_delete_test(void **state)
{

}

static void 
list_get_test(void **state)
{

}

static void 
list_get_index_test(void **state)
{
  int ctr;
  int index = 5;
  struct ListState *list_state = *state;
  List *head = list_state->list;


  ctr = 0;
  while(head!=NULL)
  {
    struct DataNode *node = head->data;
    if (ctr == index)
    {
      assert_int_equal(3, node->val);
      break;
    }
    ctr++;
    head = head->next;
  }

}

static void 
list_get_all_test(void **state)
{
  int ctr;
  int val;
  int test_data[] = {9, 5, 3, 7, 8, 3, 6, 8, 9, 3};
  struct ListState *list_state = *state;
  List *head = list_state->list;


  ctr = 0;
  while(head!=NULL)
  {
    struct DataNode *node = head->data;
    assert_int_equal(test_data[ctr], node->val);
    ctr++;
    head = head->next;
  }
}


static int
setUp(void ** state)
{
  int data[] = {9, 5, 3, 7, 8, 3, 6, 8, 9, 3};
  int ctr;
  struct ListState * list_state = (struct ListState *)malloc(sizeof(struct ListState));
  *state = list_state;

  for(ctr=0; ctr<10; ctr++)
  {
    struct DataNode * node = (struct DataNode *)malloc(sizeof(struct DataNode));
    node->val = data[ctr];
    list_state->list = list_append(list_state->list, node);
  }
  return 0;
}


static int 
tearDown(void ** state)
{
  return 0;
}

int main(int argc, char *argv[])
{
  const struct CMUnitTest tests[] = {
    cmocka_unit_test(list_append_test),
    cmocka_unit_test(list_delete_test),
    cmocka_unit_test(list_get_index_test),
    cmocka_unit_test(list_get_all_test),
    cmocka_unit_test(list_get_test),
  };

  return cmocka_run_group_tests(tests, setUp, tearDown);
}
