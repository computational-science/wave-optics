module source
  real(kind=8), parameter, private :: pi = 4.D0*atan(1.D0)

    interface


    end interface



CONTAINS

real function source_gaussian()
  source_gaussian = 0.0
end function source_gaussian

real function source_sinusoidal(freq, t, ph)
  real, intent(in) :: freq, t, ph
  source_sinusoidal = sin((2*pi*freq)*t + ph)
end function source_sinusoidal



real function source_lightning()
  source_lightning = 0.0
end function source_lightning



real function source_add_sinusoidal()

end function source_add_sinusoidal

end module source
