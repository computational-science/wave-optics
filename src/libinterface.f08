module libinterface
  use iso_c_binding
  implicit none

  interface
    function read_materials(fname) bind (C, name="parse_materials")
      use iso_c_binding, only: c_char, c_ptr
      type(c_ptr) :: read_materials
      character(kind=c_char) :: fname(*)
    end function read_materials

    function list_count(ptr) bind (C, name="list_count")
      use iso_c_binding, only: c_int, c_ptr
      type(c_ptr) :: ptr
      integer (kind=c_int) :: list_count
    end function list_count
  end interface







end module libinterface
