%option nounput
%option noinput
%{
#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <string.h>
#include <model.tab.h>
int fileno(FILE *stream);

%}


%%
[ \t\n]                 ;
"cuboid"    {return CUBOID;}

%%

int yywrap(void)
{
  return 1;
}
