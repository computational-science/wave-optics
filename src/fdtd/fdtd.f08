module fdtd
  use pml
  use abc
  use source
  implicit none


  real, private, parameter :: c = 299792458.0
  real, parameter :: pi = 4.D0*atan(1.D0)
  real, parameter :: mu0 =4*pi*1e-7
  real, parameter :: eps0 = 1/(c*c*mu0)

  real, private :: dx, dy, dz, dt
  real, private :: lx, ly, lz, lt
  integer, private :: nx, ny, nz, nt
  real, allocatable, dimension(:,:,:) :: Ex, Ey, Ez, Hx, Hy, Hz
  real :: sigma

  real, private :: freq
  real, private :: alph_x, alph_y, alph_z
  real, private :: dtbydx, dtbydy, dtbydz


  private :: fdtd_initialise
  private :: fdtd_finalise
  private :: fdtd_update_e
  private :: fdtd_write
  private :: fdtd_update_h

  character(len=128) :: output



CONTAINS


  subroutine fdtd_initialise()
    real:: wavelength


    freq = 100e6

    wavelength = c/freq
    lx = 2
    ly = 2
    lz = 2
    lt = 1e-3


    dx = wavelength/12.0
    dy = wavelength/12.0
    dz = wavelength/12.0
    dt = 0.5*sqrt(dx*dx + dy*dy + dz*dz)/c

    nt = int(lt/dt)
    nx = int(lx/dx)
    ny = int(ly/dy)
    nz = int(lz/dz)


    dtbydx = dt/dx
    dtbydy = dt/dy
    dtbydz = dt/dz

    allocate(Ex(nx,ny,nz))
    allocate(Ey(nx,ny,nz))
    allocate(Ez(nx,ny,nz))

    allocate(Hx(nx,ny,nz))
    allocate(Hy(nx,ny,nz))
    allocate(Hz(nx,ny,nz))
  end subroutine fdtd_initialise


  subroutine fdtd_finalise()
    deallocate(Ex)
    deallocate(Ey)
    deallocate(Ez)

    deallocate(Hx)
    deallocate(Hy)
    deallocate(Hz)
  end subroutine fdtd_finalise


  subroutine fdtd_update_e()
    Integer:: x, y, z

    do concurrent (z=1: nz)
      do concurrent (x=1: nx)
        do concurrent (y=1: ny)

          Ex(x,y,z) = Ex(x,y,z) + dtbydy*(Hy(x,y,z) - Hy(x,y,z)) + & 
                dtbydz*(Hz(x,y,z) - Hz(x,y,z)) + sigma * Ex(x,y,z)

          Ey(x,y,z) = Ey(x,y,z) + dtbydz*(Hz(x,y,z) - Hz(x,y,z)) + &
                dtbydx*(Hx(x,y,z) - Hx(x,y,z)) + sigma * Ey(x,y,z)

          Ez(x,y,z) = Ez(x,y,z) + dtbydx*(Hx(x,y,z) - Hx(x,y,z)) + &
                dtbydy*(Hy(x,y,z) - Hy(x,y,z)) + sigma * Ez(x,y,z)
        end do
      end do
    end do
  end subroutine fdtd_update_e

  subroutine fdtd_update_h()
    Integer:: x, y, z

    do concurrent (z=1: nz)
      do concurrent (x=1: nx)
        do concurrent (y=1: ny)

          Hx(x,y,z) = Hx(x,y,z) + dtbydz*(Ez(x,y,z) - Ez(x,y,z)) + &
                dtbydy*(Ey(x,y,z) - Ey(x,y,z))

          Hy(x,y,z) = Hy(x,y,z) + dtbydx*(Ex(x,y,z) - Ex(x,y,z)) + &
                dtbydz*(Ez(x,y,z) - Ez(x,y,z))

          Hz(x,y,z) = Hz(x,y,z) + dtbydy*(Ey(x,y,z) - Ey(x,y,z)) + & 
                dtbydx*(Ex(x,y,z) - Ex(x,y,z))

        end do
      end do
    end do
  end subroutine fdtd_update_h







  subroutine fdtd_solve()
    Integer:: timer
    call fdtd_initialise()
    Write(*,*) "Completed FDTD initialisation"

    do timer=1,nt
      call fdtd_update_e()
      call fdtd_update_h()
      call fdtd_write()
    end do

    Write(*,*) "Completed solving through FDTD"
    call fdtd_finalise()
  end subroutine fdtd_solve


  subroutine fdtd_write()

  end subroutine fdtd_write

end module fdtd
