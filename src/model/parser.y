%{
  #include<stdio.h>
  #include <stdlib.h>

  extern int yylex();
  extern int yyparse();
  void yyerror(char *s);
%}

%union {
  int num;
  float fval;
  char id;
	int token;
	char *sval;
}

%token <token> LBRACKET
%token <token> RBRACKET

%start scene

%token <num> INTEGER
%token <fval> REAL
%token <id> IDENTIFIER
%token <id> KEYWORD
%token <id> PRIMITIVE
%token <id> GROUP_OPERATION
%token <id> TRANSFORM_OPERATION
%token <id> CUBOID


%type <id> cuboid

%%

scene: object;

object   : '(' cuboid ')' { printf("CUBOID\n"); }
         ;

cuboid   : CUBOID ;
 

%%

void yyerror(char *s)
{
  exit(1);
}
