module rle

  Type rle_t
    Integer :: id
    Integer :: val
  end Type rle_t



  Type(rle_t), allocatable, dimension(:) :: model_rle


CONTAINS


  subroutine rle_get_list()

  end subroutine rle_get_list


  subroutine rle_normalise

  end subroutine rle_normalise


end module rle
