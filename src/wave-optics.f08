program waveoptics
  use iso_c_binding
  use iso_fortran_env
  use libinterface
  use fdtd
  implicit none

  integer(kind=c_int) :: nmat

  character (len=128) :: materials_file, model_file
  character (len=128) :: parameters_file, settings_file
  type(c_ptr) :: matlist
  
  namelist /files/ materials_file, model_file, settings_file, parameters_file

  Print *, "Shashank"

  if (this_image() == 1) then
    materials_file="/home/Shashank/wave-optics/examples/models/material1.yaml"//C_NULL_CHAR
    model_file="/home/Shashank/wave-optics/examples/models/model1.mod"//C_NULL_CHAR
    parameters_file="/home/Shashank/wave-optics/examples/parameters/simulation1.yaml"//C_NULL_CHAR
    settings_file="/home/Shashank/wave-optics/examples/parameters/project.yaml"//C_NULL_CHAR


    matlist = read_materials(materials_file)
    nmat = list_count(matlist)



    call fdtd_solve()


    Write(*,*) "Read ", nmat, " materials"
    write(*, nml = files)

  end if

  sync all

end program waveoptics
