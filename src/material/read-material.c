#include "material.h"
#include <yaml.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>


float
get_parameter_float(yaml_parser_t *parser)
{
  yaml_event_t event;
  if (!yaml_parser_parse(parser, &event))
  {
     fprintf(stderr, "Parser error %d\n", parser->error);
     exit(EXIT_FAILURE);
  }
  if(event.type == YAML_SCALAR_EVENT)
    return atof((const char*) event.data.scalar.value);
  else
    return 0;
}
static char *
get_parameter_string(char * dest, yaml_parser_t *parser)
{
  yaml_event_t event;
  if (!yaml_parser_parse(parser, &event))
  {
     fprintf(stderr, "Parser error %d\n", parser->error);
     exit(EXIT_FAILURE);
  }
  if(event.type == YAML_SCALAR_EVENT)
  {
    strcpy(dest, (const char *)event.data.scalar.value);
  }
  return dest;
}

int 
get_matrix(complex float mat[][3], yaml_parser_t *parser)
{
  yaml_event_t event;
  int index = 0;
  bool main_seq = 0;
  bool row_seq = 0;
  bool flag = 0;

  do 
  {
    if (!yaml_parser_parse(parser, &event))
    {
       fprintf(stderr, "Parser error %d\n", parser->error);
       exit(EXIT_FAILURE);
    }

    switch(event.type)
    {
      case YAML_NO_EVENT: 
	puts("No event!"); 
	break;
      case YAML_SEQUENCE_START_EVENT: 
	if (main_seq == 0)
	  main_seq = 1;
	else
	  row_seq = 1;
       	break;
      case YAML_SEQUENCE_END_EVENT:
	if (row_seq==1)
	  row_seq = 0;
	else
	{
	  main_seq = 0;
	  flag = 1;
	}

     	break;
      case YAML_SCALAR_EVENT: 
	{
	  float a=0, b=0;
	  char ch;

	  char buffer[80];
	  strcpy(buffer, (const char *)event.data.scalar.value);

	  if(strstr(buffer, "+"))
	  {
	    sscanf((const char *)event.data.scalar.value, "%f %c %fj", &a, &ch, &b);
	  }
	  else if(strstr(buffer, "j"))
	    b = atof(buffer);
	  else
	    a = atof(buffer);

	  mat[index/3][index%3] = a + b*_Complex_I;

	  index++;
	  break;
	}
      default:
       fprintf(stderr, "Invalid Parser error %d \n", parser->error);
       exit(EXIT_FAILURE);
    }
  }while(!flag);
  return 0;
}

void
parse_next(yaml_parser_t *parser, yaml_event_t *event)
{
  /* parse next scalar. if wrong exit with error */
  if ( !yaml_parser_parse(parser, event) ){
      printf("Parser error %d\n", parser->error);
      exit(EXIT_FAILURE);
  }
}


List*
parse_materials(const char * fname)
{
  FILE *fp = fopen(fname, "r");
  yaml_parser_t parser;
  yaml_event_t event;
  bool map_status = 0;
  bool key_status = 0;
  unsigned int mat_id;
  List * list = NULL;
  Material_t * material;

  if(!yaml_parser_initialize(&parser))
    fputs("Failed to initialize parser\n", stderr);

  if(fp == NULL)
    fprintf(stderr, "Failed to open the material file %s\n", fname);

  yaml_parser_set_input_file(&parser, fp);

  do 
  {
    if (!yaml_parser_parse(&parser, &event))
    {
       fprintf(stderr, "Parser error %d\n", parser.error);
       exit(EXIT_FAILURE);
    }

    switch(event.type)
    {
      case YAML_NO_EVENT: 
				puts("No event!"); 
				break;
      /* Stream start/end */
      case YAML_STREAM_START_EVENT: 
	puts("Parsing the materials file"); 
	break;
      case YAML_STREAM_END_EVENT: 
      	puts("Parsing completed"); 
      	break;
      /* Block delimeters */
      case YAML_DOCUMENT_START_EVENT:
       	break;
      case YAML_DOCUMENT_END_EVENT: 
      	break;
      case YAML_SEQUENCE_START_EVENT: 
	list = NULL;
       	break;
      case YAML_SEQUENCE_END_EVENT:
     	break;
      case YAML_MAPPING_START_EVENT: 
	mat_id++;
	map_status = 1;
	material = (Material_t *) malloc (sizeof(Material_t));
      	break;
      case YAML_MAPPING_END_EVENT: 
	map_status = 0;
	list = list_append(list, (void *)material);
	printf("Completed creation of material %s\n", material->name);
	material = NULL;
    	break;
      /* Data */
      case YAML_ALIAS_EVENT:  
	printf("Got alias (anchor %s)\n", event.data.alias.anchor);
       	break;
      case YAML_SCALAR_EVENT: 
	if (key_status ==0)
	{
	  if (map_status == 1)
	  {
	    if(material)
	    {
	      if(strcmp("name", (const char *)event.data.scalar.value)==0)
					get_parameter_string(material->name, &parser);
	      else if(strcmp("model", (const char *)event.data.scalar.value)==0)
					get_parameter_string(material->model, &parser);
	      else if(strcmp("eps", (const char *)event.data.scalar.value)==0)
					get_matrix(material->eps, &parser);
	      else if(strcmp("mu", (const char *)event.data.scalar.value)==0)
					get_matrix(material->mu, &parser);
	      else if(strcmp("sigma", (const char *)event.data.scalar.value)==0)
					material->sigma = get_parameter_float(&parser);
	      else if(strcmp("delta", (const char *)event.data.scalar.value)==0)
					material->delta = get_parameter_float(&parser);
	      else if(strcmp("omega", (const char *)event.data.scalar.value)==0)
					material->omega = get_parameter_float(&parser);
	      else
					printf("Got strange scalar (value %s)\n", event.data.scalar.value);
	    }
	  }
	}
	else
	{

	}
       	break;
    }
    if(event.type != YAML_STREAM_END_EVENT)
      yaml_event_delete(&event);
  } while(event.type != YAML_STREAM_END_EVENT);
  yaml_event_delete(&event);


  yaml_parser_delete(&parser);
  fclose(fp);


  return list;

}
