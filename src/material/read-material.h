#ifndef _READ_MATERIAL_H
#define _READ_MATERIAL_H

#include <material/material.h>

struct _material_t * read_materials(const char *fname);


#endif
