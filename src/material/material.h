#ifndef MATERIAL_PARSE_H
#define MATERIAL_PARSE_H

#include <complex.h>
#include <util/list.h>

struct _Material_t
{
  char name[50];
  char model[50];
  complex float eps[3][3];
  complex float mu[3][3];
  float sigma;
  float delta;
  float omega;
};


typedef struct _Material_t Material_t;


List * parse_materials(const char * fname);



#endif
