#ifndef HUFFMAN_H
#define HUFFMAN_H

struct huffman_node_t
{
  int value;
  unsigned int frequency;
  struct huffman_node_t * left, * right;
};


unsigned int huffman_get_symbol_count(unsigned int * mat, int size);
float *      huffman_get_frequencies(unsigned int *mat, int size, int nmat);
void get_huffman_matrix(unsigned int * mat, unsigned int size);

struct huffman_node_t * get_huffman_tree(float * frequncies, int nmat);


#endif

