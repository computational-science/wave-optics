#ifndef UTIL_LIST_H
#define UTIL_LIST_H

#include <util/util.h>

struct _List
{
  void *data;
  struct _List * next;
};


typedef struct _List List;

List * list_append(List *, void * data);
List * list_delete(List *, void *, compare_function compare, int flag);

void * list_get(List *, int index);
List * list_delete_index(List *, int index, int flag);

int    list_find_minimum(List *list, value_function);
int    list_find_maximum(List *list, value_function);
int list_count(List * list);

#endif
