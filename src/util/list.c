#include "list.h"
#include <float.h>
#include <stdio.h>
#include <stdlib.h>

int list_count(List * list)
{
  List *head;
  int count = 0;
  head = list;
  while(head != NULL)
  {
    head = head->next;
    count ++;
  }
  return count;
}

List * list_append(List *list, void * data)
{
  List * node;
  if (list == NULL)
  {
    node = (List *) malloc(sizeof(List));
    node->data = data;
    node->next = NULL;
    return node;
  }
  else
  {
    List *head;
    List * cur;
    head = list;
    while(head != NULL)
    {
      cur = head;
      head = head->next;
    }
    node = (List *) malloc(sizeof(List));
    cur -> next = node;
    node->data = data;
    node->next = NULL;
    return list;
  }
}

List * 
list_delete(List *list, void * data, compare_function compare, int flag)
{
  List * head = list;
  List * prev = list;
  while(head!=NULL)
  {
    int val = compare(head->data, data);

    if(val == 0)
    {
      if (head == list)
      {
        list = list->next;
        if(flag)
          free(head->data);
        free(head);
      }
      else
      {
        List * tmp = head;
        prev->next = head->next;
        if(flag)
          free(head->data);
        free(head);
      }
    }

    prev = head;
    head = head->next;
  }

  return list;
}


int  
list_find_minimum(List *list, value_function vfun)
{
  List *head;
  int index = 0;
  float min_val = FLT_MAX;
  int min_index=-1;

  if (list==NULL)
    return -1;

  while(head != NULL)
  {
    float val = vfun(head->data);
    if (val < min_val)
    {
      min_val = val;
      min_index = index;
    }
    index++;
  }
  return min_index;
}

int
list_find_maximum(List *list, value_function vfun)
{
  List *head;
  int index = 0;
  float max_val = -FLT_MAX;
  int max_index=-1;

  if (list==NULL)
    return -1;

  while(head != NULL)
  {
    float val = vfun(head->data);
    if (val < max_val)
    {
      max_val = val;
      max_index = index;
    }
    index++;
  }
  return max_index;
}

void *
list_get(List *list, int index)
{
  int ctr;
  List *head;
  void *data;

  ctr = 0;
  while(head != NULL)
  {
    if(ctr == index)
    {
      data = head->data;
      break;
    }
    ctr++;
    head = head->next;
  }
  return data;
}

List *
list_delete_index(List *list, int index, int flag)
{
  int ctr;
  List *head, *prev;
  head = list;
  prev = list;
  ctr = 0;

  while(head != NULL)
  {
    if(ctr == index)
    {
      if (head == list)
      {
        list = list->next;
        if(flag)
          free(head->data);
        free(head);
        break;
      }
      else
      {
        List * tmp = head;
        prev->next = head->next;
        if(flag)
          free(head->data);
        free(head);
      }
      break;
    }
    ctr++;
    prev = head;
    head = head->next;
  }
  return list;
}
