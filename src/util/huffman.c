#include "huffman.h"
#include <stdio.h>
#include <stdlib.h>
#include <util/list.h>
#include <math.h>


float get_freq(void *data)
{
  struct huffman_node_t *node = (struct huffman_node_t *)data;
  return node->frequency;
}

unsigned int 
huffman_get_symbol_count(unsigned int * mat, int size)
{
  unsigned int nmat = 0;
  unsigned int ctr;

  for(ctr=0; ctr<size; ctr++)
  {
    if (*(mat + ctr) > nmat)
    {
      nmat = *(mat + ctr);
    }
  }
  return nmat+1;
}

float *     
huffman_get_frequencies(unsigned int *mat, int size, int nmat)
{
  unsigned int ctr;
  unsigned mat_id;
  float *frequencies;

  frequencies = (float *) malloc (sizeof(float)*nmat);

  for(ctr = 0; ctr < size; ctr++)
  {
    mat_id = *(mat + ctr);
    frequencies[mat_id]++;
  }
  for(ctr=0; ctr<nmat; ctr++)
  {
    frequencies[ctr]= frequencies[ctr]/(float)size;
  }

  return frequencies;
}

struct huffman_node_t *
get_huffman_tree(float * frequencies, int nmat)
{
  int ctr;
  struct huffman_node_t *root;
  List *list=NULL;
  value_function vfun;
  vfun = get_freq;

  for(ctr = 0; ctr < nmat; ctr++)
  {
    struct huffman_node_t *node;
    node = (struct huffman_node_t *) malloc(sizeof(struct huffman_node_t));
    node->frequency = frequencies[ctr];
    node->value = ctr;
    node->left = NULL;
    node->right = NULL;
    list = list_append(list, node); 
  }

  while(list_count(list) <= 1)
  {
    int min1, min2;
    struct huffman_node_t * node1, *node2, *node;


    min1 = list_find_minimum(list, vfun);
    node1 = list_get(list, min1);
    list = list_delete_index(list, min1, 0);

    min2 = list_find_minimum(list, vfun);
    node2 = list_get(list, min2);
    list = list_delete_index(list, min2, 0);


    node = (struct huffman_node_t *)malloc(sizeof(struct huffman_node_t));

    node->right = node1;
    node->left = node2;
    node->frequency = node1->frequency + node2->frequency;

    list = list_append(list, node);
  }
  
  /* root is the root node for the huffman tree */
  root = list->data;
  list_delete_index(list, 0, 0);
  return root;

}


void get_huffman_matrix(unsigned int * mat, unsigned int size)
{
  unsigned int ctr;
  unsigned mat_id;
  float *frequencies;
  List * freq_list;
  unsigned int nmat;
  struct huffman_node_t *root;
  
  nmat = huffman_get_symbol_count(mat, size);

  frequencies = (float *) malloc (sizeof(float)*nmat);
  frequencies = huffman_get_frequencies(mat, size, nmat);

  root = get_huffman_tree(frequencies, nmat);

  free(frequencies);
}
