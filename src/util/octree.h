#ifndef OCTREE_H
#define OCTREE_H

struct octree_t
{
  struct octree_t * subnodes[8]; /* 8 pointers = 64 bytes*/
  float center[3]; /* 3 floats = 24 bytes */
  float size[3]; /* 3 floats = 24 bytes */
  void *data; /* 8 bytes */
  /* Total 120 bytes for a node */
};



struct octree_t * create_octree(void *);

struct octree_t * octree_split(struct octree_t *);



#endif

