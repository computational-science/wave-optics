# Wave Optics

*Wave Optics* is an open source software for solving maxwell's equations in 3-dimensions.
The software is fully customizable using _scheme_ and _python_ scripts. Powerful graphic 
visualizations help in analysing the results effectively.

## Getting Started

### Prerequisites

.Prerequisites
 * libyaml - For storinng and parsing setting/parameter files as YAML files
 * OpenMPI - For parallel programming 
 * libpng - For ray tracing output
 * theora/ogg - For storing the results as videos
 * glfw - For OpenGL based visualizations


### Installing

The following steps guide in the installation of the software. Before
this step, ensure that all prerequisite software is installed.

```
$ tar -xf wave-optics.tar.gz
$ cd wave-optics
$ mkdir build
$ cd build
$ ../configure
$ make -j 4
$ make install
```

_Wave Optics_ is now installed in default location.


### Testing

It is advisable ro run the tests before using the software. This helps
in ensuring that the software runs as designed with the external
libraries. 

Run the tests as follows:

```
make test
make coverage
```

### Usage

Now tha the software and the tests have sucessfully run, it is time to
launch our software. 

Run the program as follows:

```
cafrun ./wave-optics -m model.mod -t material.json -p simulation.json -r results.json -s settings.json
```
or
```
cafrun ./wave-optics --model model.mod --material material.json --parameters simulation.json --results results.json --settings settings.json
```

To get help with the commands
```
./wave-optics -h
```
or
```
./wave-optics --help
```

#### Documentation
The project documentation is maintained at http://bandari.in

## Deployment

## Contributing

See [CONTRIBUTING](CONTRIBUTING.md) for details on contributing to the project.

## Release Process
See [RELEASE](doc/RELEASE.md) for details on release process.

## Versioning
*Wave Optics* uses Semver for software versioning.

* Major release - x
* Minor release - x.y
* Patch release - x.y.z


As the software follows incremental and iterative approach for the development, the below table gives a summary

| Code Status																		| Stage 				| Rule											| Example |
|-----------------------------------------------|---------------|---------------------------|---------|
| First Release																	| New Product		| Start with 1.0.0					|	1.0.0		|
| Bug fixes, other minor changes								| Patch release	| Increment the third digit	| 1.0.1		|
| New feature, doesn't break existing features	|	Minor release	| Increment the middle digit| 1.1.1		|
| Changes that break backword compatibility			| Major release | Increment the first digit	| 2.0.0		|

## Authors

Shashank Bandari <shashank@bandari.in>

## License

See [LICENSE](LICENSE) for license details of this project.

## Acknowledgements
None so far
